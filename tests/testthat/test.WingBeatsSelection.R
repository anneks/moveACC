context("WingBeatsSelection")
test_that("WingBeatsSelection",{
  data(waveDF)
  expect_equal(class(WingBeatsSelection(waveDF, indName="4159", forclustering= c("amplitude","odbaAvg"), minbeat=2,maxbeat=5)),'data.frame')
})
