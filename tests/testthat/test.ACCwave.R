context("ACCwave")
test_that("ACCwave",{
  data(ACCtable)
  expect_equal(class(ACCwave(ACCtable,transformedData=F)),'data.frame')
  expect_equal(length(colnames(ACCwave(ACCtable,transformedData=F))),92)
})