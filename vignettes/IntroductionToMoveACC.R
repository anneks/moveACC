## ----setup, include = FALSE---------------------------------------------------
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)

## ----message=FALSE------------------------------------------------------------
library("move")
library("moveACC")

## ----eval=F-------------------------------------------------------------------
#  ## get 2 example individuals from movebank.org
#  creds <-  movebankLogin()
#  ACCtable <- getMovebankNonLocationData(study=404939825, sensorID=2365683,
#                                         animalName=c("1608","4159"), login=creds)

## -----------------------------------------------------------------------------
data(ACCtable) # example data loaded to be able to create this vignette

## -----------------------------------------------------------------------------
ACCtimeRange(ACCtable, units="days")

## -----------------------------------------------------------------------------
BurstSamplingScedule(ACCtable, showProgress=F)

## -----------------------------------------------------------------------------
sensitiv <- data.frame(TagID=1608, sensitivity="low")
transfDF <- TransformRawACC(ACCtable, sensitivity.settings=sensitiv, 
                          units="g", showProgress=F)

## -----------------------------------------------------------------------------
cailbT1 <- data.frame(TagID=4159,nx=2040,ny=2050,nz=2045,
                      cx=0.00196,cy=0.00195,cz=0.001955)
cailbT2 <- data.frame(TagID=1608,nx=2039,ny=2040,nz=2042,
                      cx=0.00194,cy=0.00194,cz=0.001955)
cailb <- rbind(cailbT1,cailbT2)

transfDF <- TransformRawACC(df=ACCtable, calibration.values=cailb, units="g",
                          showProgress=F)

## -----------------------------------------------------------------------------
transfDF <- TransformRawACC(df=ACCtable, sensitivity.settings=sensitiv, 
                          calibration.values=cailbT1, units="g", showProgress=F)

## -----------------------------------------------------------------------------
myconstants <- data.frame(TagID=c(1608,4159),intercept=2040,slope=0.00194)
transfDF <- TransformRawACC(df=ACCtable, calibration.values=myconstants, 
                          units="g", showProgress=F)

## ----fig.height=3, fig.width=8------------------------------------------------
PlotAccData(df=ACCtable,indName="1608",bursts=c(2:6), interactivePlot=F)

## ----fig.height=3, fig.width=8------------------------------------------------
PlotAccDataTIME(df=ACCtable,indName="1608",bursts=c(2:6), interactivePlot=F)

## -----------------------------------------------------------------------------
statsDF <- ACCstats(df=transfDF)
str(statsDF)

## ----"a"----------------------------------------------------------------------
## with raw data
waveDFraw <- ACCwave(ACCtable,transformedData=F, showProgress=F)
str(waveDFraw)

## ----"b"----------------------------------------------------------------------
## with transformed data
waveDF <- ACCwave(transfDF,transformedData=T, showProgress=F)
str(waveDF)

## ----fig.height=5, fig.width=8------------------------------------------------
clusterPlot(waveDF, indName="4159", cluster=F)

## ----fig.height=5, fig.width=8------------------------------------------------
clusterPlot(waveDF, indName="4159", cluster=T,
            forclustering= c("amplitude","odbaAvg"))

## ----fig.height=3, fig.width=3------------------------------------------------
wingBeatsPlot(dfw=waveDF, indName="4159",
              forclustering= c("amplitude","odbaAvg"), interactivePlot=F)
wingBeatsPlot(dfw=waveDF, indName="4159",
              forclustering= c("amplitude","odbaAvg"), xlim=c(3,5), interactivePlot=F)

## ----fig.height=3, fig.width=3------------------------------------------------
wingBeatsHist(dfw=waveDF, indName="4159",
              forclustering= c("amplitude","odbaAvg"), interactivePlot=F)
wingBeatsHist(dfw=waveDF, indName="4159",
              forclustering= c("amplitude","odbaAvg"), xlim=c(3,5), interactivePlot=F)

## -----------------------------------------------------------------------------
wingbeatsDF <- WingBeatsSelection(waveDF, indName="4159",
                                      forclustering= c("amplitude","odbaAvg"),
                                      minbeat=2,maxbeat=5)
str(wingbeatsDF)


