[![](http://www.r-pkg.org/badges/version/moveACC)](https://CRAN.R-project.org/package=moveACC)
[![pipeline](https://gitlab.com/anneks/moveACC/badges/master/pipeline.svg)](https://gitlab.com/anneks/moveACC/pipeline)
                                                  
# Installation instructions

## Install the development version
``` R
library('devtools')
install_git('https://gitlab.com/anneks/moveACC.git', build = TRUE, build_opts = c("--no-resave-data", "--no-manual"), build_vignettes=T)
```

# Bugs and Feature requests

Bug reports and feature requests are very welcome and can be uploaded to the gitlab issue tracker (https://gitlab.com/anneks/moveACC/issues)



# Introduction and overview of the different functions

The moveACC package resulted from functions written to do the analysis for the paper _O'Mara, M. Teague, et al. "Overall dynamic body acceleration in straw-colored fruit bats increases in headwinds but not with airspeed." Frontiers in Ecology and Evolution 7 (2019): 200. https://doi.org/10.3389/fevo.2019.00200_. Most functions of this package are optimized for eObs loggers. The wingbeat frequency calculation was optimized for the straw-coloured fruit bat *Eidolon helvum* data, and have not yet been tested on other species with other flight behaviors or logger settings. Here an overview of the available functions in this package:


## Download acceleration data from Movebank
For the illustration of this vignette, the acceleration data of 2 individuals are downloaded from Movebank, using the function available in the package *move*. See help file of function *getMovebankNonLocationData* for details.
```r
library("move")
library("moveACC")
```
```r
## get 2 example individuals from movebank.org
data(ACCtable) # this is the data set used for the following examples
```
```r
## to download acceleration data from Movebank:
creds <-  movebankLogin()
ACCtable <- getMovebankNonLocationData(study="Study X", sensorID=2365683, login=creds)
?getMovebankNonLocationData ## see help for details
```


## Data inspection
First, lets inspect the data

### Get time range of the data
```r
ACCtimeRange(ACCtable, units="days")
```
### Get sampling details of acceleration bursts
This overview is important, because although settings were made when deploying the logger, they can be modified via the base station, or can have some errors while being recorded. If some bursts are very short, e.g. under 1 sec or very few secs (in such a short time no behavior can be measured), these bursts should maybe be eliminated from the further analysis. If the data set is large, this may take a while

```r
BurstSamplingScedule(ACCtable)
```


## Transform raw acceleration data into "g" of "m/s²"
This function is optimized for eObs tags, but can be used for acceleration data from other manufacturers that follow the same basic calculation for the transformation.
eObs tags, and also from other manufacturers, provide acceleration data in raw digital readings, these raw values can be converted to physical units e.g. "g" or "m/s²". For this conversion 2 constants (slope and intercept) per sensor and axis are necessary. These constants should be calculated for each sensor before deployment (see manual of manufacturer). In case this has not been calculated, eObs provides default values for these constants. Currently (as of August 2018) there are 3 generations of eObs tags, which have different values for the needed constants. Generation 1 include tags with ID lower than 2241 and have to be set to low or high sensitivity when being deployed. Generation 2 include tags with IDs between 2242 and 4117; and generation 3 tags with IDs higher than 4118. In tags of generation 2 and 3 the sensitivity is fixed to "low" and cannot be modified. The orientation of the Y-axis in generation 1 is different to generation 2 and 3. For more information check the tag user manual of e-obs GmbH.
In a dataset with several tags the transformation can be made:

### Using only the default slope and intercept values (this only is true for eObs tags)
In this example, on of the tags is of generation 1, therefore the sensitivity has to be set for that tag.
```r
sensitiv <- data.frame(TagID=1608, sensitivity="low")
transfDF <- TransformRawACC(ACCtable, sensitivity.settings=sensitiv, units="g")
```

### Using own calibration values for all or some tags

Calibration values for all tags can be provided
```r
cailbT1 <- data.frame(TagID=4159, nx=2040, ny=2050, nz=2045, cx=0.00196, cy=0.00195, cz=0.001955)
cailbT2 <- data.frame(TagID=1608, nx=2039, ny=2040, nz=2042, cx=0.00194, cy=0.00194, cz=0.001955)
cailb <- rbind(cailbT1,cailbT2)

transfDF <- TransformRawACC(df=ACCtable, calibration.values=cailb, units="g")
```
Or just for some of them
```r
transfDF <- TransformRawACC(df=ACCtable, sensitivity.settings=sensitiv, calibration.values=cailbT1, units="g")
```


### Using user defined slope and intercept values for all tags (useful for data from other manufacturers)
```r
myconstants <- data.frame(TagID=c(1608,4159), intercept=2040, slope=0.00194)
transfDF <- TransformRawACC(df=ACCtable, calibration.values=myconstants, units="g")
```



## Visualize the acceleration data
With this function it is possible to reproduce the same plot as can be seen with the acceleration viewer from Movebank. Either for the raw or for the transformed data.

```r
PlotAccData(df=ACCtable,indName="1608", bursts=c(2:6))
```

And also to visualize the acceleration data in relation to time.
```r
PlotAccDataTIME(df=ACCtable,indName="1608", bursts=c(2:6))
```



## Obtain general descriptive statistics of the acceleration data
This function returns a set of descriptive statistics per axis and acceleration burst, like e.g. mean value per axis, variance per axis, mean ODBA, frequency, amplitude, etc.

```r
statsDF <- ACCstats(df=transfDF)
str(statsDF)
```


## Extracting wingbeat frequency
***Note: This function and the following workflow has been optimized for a dataset of acceleration data of the fruit bat *Eidolon helvum*. No guarantee that it will work as good for other species.***


The *ACCwave* function uses a FFT (Fast Fourier Transformation) to extract the wave of the acceleration data. Tags can be attached to an animal in a way that it always maintains the same position (e.g. backpack, glued) or in a way that it can move freely and change position (e.g. collar, legband). For this reason, the function does a PCA (principal components analysis) on the 3 axis, and calculates the FFT on PC1 which will contain the dominant frequency of the burst, which corresponds to the wingbeat frequency.

The function returns a table with the potential wingbeats/sec for each acc burst, along with a series of other variables either extracted from the wave or from the acc data that will be used to determine which burst actually corresponds to flying and which not (see 'Values' in help file). Even if the animal is not moving, there still can be extracted a wave from the data, which will have a frequency. Therefore the several steps have to be followed to obtain the behavioral classification of flapping and non flapping for each burst:


**1.** The raw or the transformed acc data can be used:
```r
## with raw data
waveDFraw <- ACCwave(ACCtable, transformedData=F)
str(waveDFraw)
```

```r
## with transformed data
waveDF <- ACCwave(transfDF, transformedData=T)
str(waveDF)
```


**2.** Plot all variables against wingbeats/sec, to find out which variables will be best fitting for the clustering
```r
clusterPlot(waveDF, indName="4159", cluster=F)
```


**3.** Choose 1 or more variables to cluster the data, and plot again all variables against wingbeats/sec to see how well the clustering worked
```r
clusterPlot(waveDF, indName="4159", cluster=T, forclustering= c("amplitude","odbaAvg"))
```


**4.** Plot the wingbeats/sec against the burstID. This is a interactive plot. This plot allows to identify outliers and also to identify the bursts that my be doing something odd. Single bursts can than be plotted using the function "PlotAccData"
```r
wingBeatsPlot(dfw=waveDF, indName="4159", forclustering= c("amplitude","odbaAvg"))
wingBeatsPlot(dfw=waveDF, indName="4159", forclustering= c("amplitude","odbaAvg"), xlim=c(3,5))
```


**5.** Do histogram of distribution of wingbeats/sec to identify outliers
```r
wingBeatsHist(dfw=waveDF, indName="4159", forclustering= c("amplitude","odbaAvg"))
wingBeatsHist(dfw=waveDF, indName="4159", forclustering= c("amplitude","odbaAvg"), xlim=c(3,5))
```


**6.** Once the best variables for clustering have been identified, and the realistic range of wingbeat frequency, a behavioral category ("Flapping", "NotFlapping") can be added to the table
```r
wingbeatsDF <- WingBeatsSelection(waveDF, indName="4159", forclustering= c("amplitude","odbaAvg"), 
                                  minbeat=2, maxbeat=5)
str(wingbeatsDF)

```
